const fs = require('fs');
const FILE_PATH = './inputFiles';
const dirFilles = fs.readdirSync(FILE_PATH);


const csvRowToSubMapper = row => {
    const [id, url, name] = row.split(',');
    return {
        service_id: 0,
        url,
        name
    }
};

const readJsonFile = fileName => JSON.parse(fs.readFileSync(`${FILE_PATH}/${fileName}`, 'utf-8')).subscriptions;
const readCsvFile = fileName => {
    const fileData = fs.readFileSync(`${FILE_PATH}/${fileName}`, 'utf-8');
    const [header, ...rows] = fileData.split('\n');
    return rows.map(csvRowToSubMapper);
};

const isCsv = fileName => fileName.endsWith('.csv');
const getSubscriptionId = sub => sub.url?.split('/')[sub.url?.split('/').length-1];
const subSetToArray = subSet => Object.keys(subSet).map(sub => subSet[sub])



const files = dirFilles.filter(file => file.endsWith('.json') || file.endsWith('.csv'));

const idsByFile = {};
const missingSubsInCsv = {};
const allSubscriptionsSet = {};

const readFileData = fileName => {
    const fileData = isCsv(fileName) ? readCsvFile(fileName) : readJsonFile(fileName);
    console.log(`found ${fileData.length} on file: ${fileName}`);

    const subscriptionIds = fileData.map(sub => {
        allSubscriptionsSet[getSubscriptionId(sub)] = sub
        return getSubscriptionId(sub)
    });
    idsByFile[fileName] = subscriptionIds;
};
files.forEach(readFileData);

const csvDiff = () => {
    const csvFiles = Object.keys(idsByFile).filter(isCsv);
    csvFiles.forEach(file => {
        const toDiff = idsByFile[file];
        missingSubsInCsv[file] = subSetToArray(allSubscriptionsSet)
            .filter(sub => !toDiff.includes(getSubscriptionId(sub)));
    })
}

csvDiff();

const resultData = subSetToArray(allSubscriptionsSet).map(sub => {
    return {
        ...sub,
        url: `https://www.youtube.com/channel/${getSubscriptionId(sub)}`
    }
});


fs.writeFileSync('./subscriptions.json', JSON.stringify(resultData));
fs.writeFileSync('./missingSubsInCsvFiles.json', JSON.stringify(missingSubsInCsv));